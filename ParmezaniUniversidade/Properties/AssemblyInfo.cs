﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As informações gerais sobre um assembly são controladas através do seguinte
// conjunto de atributos a seguir. Altere esses valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("ParmezaniUniversidade")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ParmezaniUniversidade")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Definir ComVisible como falso torna não visíveis os tipos neste assembly
// para componentes COM. Caso precise acessar um tipo neste assembly a partir de
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// A GUID a seguir será referente à ID do typelib se este projeto for exposto ao COM
[assembly: Guid("af3b9605-47cc-4483-96c2-2eb648700f1b")]

// As informações de versão de um assembly consistem nos seguintes quatro valores:
//
//      Versão Principal
//      Versão Secundária
//      Número da Versão
//      Revisão
//
// É possível especificar todos os valores ou definir como padrão os números de revisão e de versão
// usando o '*' como mostrado abaixo:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
